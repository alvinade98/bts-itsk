<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Models\User;

class AuthController extends Controller
{
    function prosesLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->role == 'mahasiswa') { // untuk mahasiswa
                return redirect(''); // page user mahasiswa

            } elseif (Auth::user()->role == 'dosen_pembimbing') { // untuk dosen pembimbing
                return redirect(''); // page user dosen_pembimbing

            } else { // untuk admin
                return redirect(''); // page user admin
            }
        } else {
            return redirect('') // page login
                ->withErrors(["Login Gagal"])->withInput();
        }
    }
}
